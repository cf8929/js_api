var canvas = document.querySelector('#myCanvas'),
    context = canvas.getContext('2d'),
    flag = false,
    cw = document.querySelector('#range1'),
    w = document.querySelector('#span1');

cw.addEventListener("input",function(){
   w.innerHTML = this.value;
})

canvas.addEventListener("mousedown",function(evt){
    flag = true;
    context.beginPath();
    // 線條顏色
    context.strokeStyle = document.querySelector('#color1').value; //$('#color1').val()
    // 線條寬度
    context.lineWidth = cw.value;
    
    context.moveTo(evt.offsetX, evt.offsetY)
    // console.log(evt.offsetX + "," + evt.offsetY)
})

canvas.addEventListener("mousemove",function(evt){
    if(flag){
        context.lineTo(evt.offsetX,evt.offsetY);
        context.stroke();
    }
})
canvas.addEventListener("mouseup",function(evt){
    flag = false;
})

var theFile = document.querySelector('#file1')
theFile.addEventListener('change',function(){
    // this.files[0] 就是File物件

    //透過FileReader物件來讀圖
    var reader = new FileReader();
    reader.readAsDataURL(this.files[0])
    reader.addEventListener('load',function(evt){
        var imageObj = new Image();
        imageObj.src = evt.target.result;
        imageObj.addEventListener('load',function(){
           context.drawImage(imageObj,0,0);
        })
    })
    
})

