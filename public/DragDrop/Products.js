﻿// 將數字轉成千分位
function commafy(num) {
    num = num + ""; //轉成字串
    var re = /(-?\d+)(\d{3})/ //regular expression
    while (re.test(num)) {
        num = num.replace(re, "$1,$2") //$1,$2可以想像為regexp的兩個group的容器
    }
    return num;
}
//解除千分位 value.replace(/[,]+/g,"");

//找到所有的商品
items = document.querySelectorAll('#products div.item')
for (var i = 0, max = items.length; i < max; i++) {
    items[i].addEventListener("mouseover", function () {
        this.getElementsByTagName("div")[0].style.display = 'block';
    }, false);
    items[i].addEventListener("mouseout", function () {
        this.getElementsByTagName("div")[0].style.display = 'none';
    }, false);

    //todo 被拖曳的元素需要有draggable="true"的設定，檢查看看每個商品是否有此屬性
    //todo 對於每個商品設定dragstart事件，並將其id儲存在dataTransfer中
    items[i].addEventListener('dragstart', function (evt) {
        evt.dataTransfer.setData('text/plain', this.id);
    })
}

//購物車區
var cart = document.getElementById("cart");

//要將商品拖曳到購物車區
//todo 設定dragover事件，使用preventDefault()防止預設動作發生
cart.addEventListener("dragover", function (evt) {
    evt.preventDefault();
})

//todo 設定drop事件，呼叫addToCart function
cart.addEventListener("drop", addToCart);



function addToCart(event) {
    //todo 使用preventDefault()防止預設動作發生
    //todo 使用stopPropagation()防止事件氣泡現象
    event.preventDefault();
    event.stopPropagation();


    //todo 從dataTransfer物件中取出(getData)之前存進去的產品Id
    var id = event.dataTransfer.getData('text/plain');
    alert(id);


    //todo 讀出商品編號、商品名稱及商品價格
    var item = document.querySelector('#' + id);
    console.log(id);
    console.log(item.querySelector('p:first-child').textContent);
    console.log(item.querySelector('p:nth-child(2)>span:nth-child(2)').textContent);

    //jQuery寫法
    //$(item).find('p:first-child').text();
    //$(item).find('p:nth-child(2)>span:nth-child(2)').text();

    //使用者要購買的資料需要儲存起來，數量預設是1
    //判斷購物車是否有此項產品，有數量加1

    //todo 用localstorage來儲存使用者購買的商品
    theValue = localStorage.getItem(id) //key:value {}
    //{"itemName":itemName, "qty":qty, "price":price}
    if (theValue) {
        //購物車有此商品，數量+1
        qty = JSON.parse(theValue).qty + 1;
        item = {
            "itemName": itemName,
            "qty": qty,
            "price": price
        };
        localStorage.setItem(id, JSON.stringify(item));
    } else {
        item = {
            "itemName": itemName,
            "qty": 1,
            "price": price
        };
        localStorage.setItem(id, JSON.stringify(item));
    }
    //讀出localStorage的資料
    showCart();

}


function showCart() {
    //todo 從localstorage中讀出資料顯示在購物車中
    myCart = document.querySelector('.table');
    //清除葉面上購物車的資料
    while (myCart.hasChildNodes()) {
        myCart.removeChild(myCart.lastChild);
    };
    docFrag = document.createDocumentFragment();
    total = 0;
    for (var i = 0, max = localStorage.length; i < max; i++) {
        id = localStorage.key(i); //key(0)回傳keyname
        item = JSON.parse(localStorage.getItem(id));

        //<td></td>
        name = item.itemName;
        var cell1 = document.createElement("td"); //<td></td>
        var txt1 = document.createTextNode(name); //CASIO Exslim PRO ....
        cell1.appendChild(txt1); //把前兩行組合起來

        //<td></td>
        qty = item.qty;
        var cell2 = document.createElement("td");
        var txt2 = document.createTextNode(qty);
        cell2.appendChild(txt2); //把前兩行組合起來

        //<td></td>
        price = item.price;
        var cell3 = document.createElement("td");
        var txt3 = document.createTextNode(price);
        cell3.appendChild(txt3); //把前兩行組合起來

        var subtotal = price.replace(/[,]+/g, "") * qty;
        var cell4 = document.createElement("td");
        var txt4 = document.createTextNode(subtotal);
        cell4.appendChild(txt4); //把前兩行組合起來

        var row = document.createElement('tr');
        row.appendChild(cell1);
        row.appendChild(cell2);
        row.appendChild(cell3);
        row.appendChild(cell4);


        docFrag.appendChild(row);
        // myCart.appendChild(row)
        total += subtotal;


    };
    myCart.appendChild(docFrag);
    document.querySelector('#total>span').innerHTML = commafy(total);
};

showCart();

btn = document.querySelector('#clear');
btn.addEventListener('click', function(){
    localStorage.clear;
    showCart();
})