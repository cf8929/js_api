var cacheName = 'v2';

self.addEventListener('install', function (event) {
    var urlToCache = [
        '/Drawingt/drawing.html',
        '/Drawingt/drawing.css',
        '/Drawingt/drawing.js'
    
    ];
    event.waitUntil(
        //建立或開幾名稱為v1的cache storage
        caches.open(cacheName).then(function (cache) {
            return cache.addAll(urlToCache);
        })
    )
})

self.addEventListener('fetch', function (event) {
    event.respondWith(
        //瀏覽的是sw.html, event.request => sw.html
        //到cache storage比對有沒有sw.html
        //會將比對到的網頁傳給response這個程式
        caches.match(event.request).then(function (response) {
            //response表示比對到的網頁
            if (response) {
                return response;
            }
            //如果比對不到就透過fetch()重新到server上要求此網頁
            return fetch(event.request)
        })
    )
})

//刪除舊的cache storage
self.addEventListener('activate',function(event){
    event.waitUntil(
        caches.keys().then(function(names){
              return Promise.all(names.map(function(name){
                   //name => v1
                   if(name != cacheName){
                       return caches.delete(name)
                   }
               }))
        })
    )
})