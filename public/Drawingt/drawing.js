var canvas = document.querySelector('#myCanvas'),
    context = canvas.getContext('2d'),
    flag = false,
    cw = document.querySelector('#range1'),
    c = document.querySelector('#color1'),
    w = document.querySelector('#span1'),
    btnSave = document.querySelector('#button1'),
    btnClear = document.querySelector('#button2'),
    myImg = document.querySelector('#img1')

//從localStorage載入圖片到canvas
var str_base64 = localStorage.getItem('myCanvas');
//判斷localStorage裡有沒有圖
if (str_base64 != null) {
    // console.log(str_base64)
    var imageObj = new Image();
    imageObj.src = str_base64;
    imageObj.addEventListener('load', function () {
        context.drawImage(imageObj, 0, 0);
    })
}


cw.addEventListener("input", function () {
    w.innerHTML = this.value;
})

canvas.addEventListener("mousedown", function (evt) {

    // send(evt.offsetX, evt.offsetY, 'mousedown', c.value, cw.value);

    flag = true;
    context.beginPath();
    // 線條顏色
    context.strokeStyle = c.value; //$('#color1').val()
    console.log(c.value);
    // 線條寬度
    context.lineWidth = cw.value;

    context.moveTo(evt.offsetX, evt.offsetY)

    // console.log(evt.offsetX + "," + evt.offsetY)

})

canvas.addEventListener("mousemove", function (evt) {
    if (flag) {
        context.lineTo(evt.offsetX, evt.offsetY);
        // send(evt.offsetX, evt.offsetY, 'mousemove', c.value, cw.value);
        context.stroke();
    }
})
canvas.addEventListener("mouseup", function (evt) {
    //send('mouseup');
    flag = false;
})

var theFile = document.querySelector('#file1')
theFile.addEventListener('change', function () {
    // this.files[0] 就是File物件

    //透過FileReader物件來讀圖
    var reader = new FileReader();
    reader.readAsDataURL(this.files[0])
    reader.addEventListener('load', function (evt) {
        var imageObj = new Image();
        imageObj.src = evt.target.result;
        imageObj.addEventListener('load', function () {
            context.drawImage(imageObj, 0, 0);
        })
    })

})

//清除CANVAS
btnClear.addEventListener('click', function () {
    //location.reload();
    context.clearRect(0, 0, canvas.width, canvas.height)

})

//儲存canvas
btnSave.addEventListener('click', function () {
    //將canvas內容顯示在img標籤中
    myImg.src = canvas.toDataURL("image/png");

    //如果網路連線中
    if (navigator.onLine) {
        //用ajax將canvas上傳到server上
        var formData = new FormData();
        var canvasData = canvas.toDataURL("image/png").replace("data:image/png;base64,", "");
        formData.append("id", new Date().getTime());
        formData.append("imageData", canvasData)

        fetch('/base64', {
            'method': 'post',
            'body': formData
        }).then(function (response) {
            return response.text();
        }).then(function (data) {
            alert(data);
            //清除localStorage及canvas中的資料
            localStorage.removeItem("myCanvas");
            context.clearRect(0, 0, canvas.width, canvas.height);
        })
        //如果網路離線中   
    } else {
        //用client端來儲存canvas - localstorage
        localStorage.setItem("myCanvas", canvas.toDataURL("image/png"));
    }


})

// // ======== websocket ========

// //todo1 連線到 ws://localhost:8080/ websocket
// var socket = new WebSocket("ws://localhost:8080");


// function send(x, y, type, c, w) {
//     var jsonObj = { x: x, y: y, type: type, color: c, width: w };
//     //todo2 將jsonOjb資料傳給Socket Server
//     socket.send(JSON.stringify(jsonObj));
// };

// //todo3 在message事件中，來接收Socket Server回傳的結果
// socket.addEventListener("message", function (event) {
//     //todo4 將Socket Server回傳的結果轉成Json物件
//     var data = JSON.parse(event.data);  
//     if (data.type === "mousedown") {
//         context.beginPath();
//         context.strokeStyle = data.color;
//         context.lineWidth = data.width;
//         context.moveTo(data.x, data.y);

//     } else if (data.type === "mousemove") {

//         context.lineTo(data.x, data.y);
//         context.stroke();

//     } else {
//     }
// }, false);