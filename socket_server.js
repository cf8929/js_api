var WebSocketServer = require('websocket').server;
var http = require('http');
var clients = [];

var server = http.createServer(function (request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});
server.listen(8080, function () {
    console.log((new Date()) + ' Server is listening on port 8080');
});

//建立SocketServer
wsServer = new WebSocketServer({
    httpServer: server,
});

//socketServer request事件
//client 連線到socketServer會觸發request事件
wsServer.on('request', function (request) {
    //接收連線
    var connection = request.accept(null, request.origin);
    //將每個連進來的使用者存進clients的陣列中
    clients.push(connection);
    console.log((new Date()) + ' Connection accepted.');
    //message事件。接收使用者傳進來的資料
    connection.on('message', function (message) {
        console.log('Received Message' + message.utf8Data);
        //接資料傳給使用者
        // connection.sendUTF(message.utf8Data + "From Server")

        //將資料廣播給所有聯近來的使用者
        for(var i=0,max=clients.length;i<max;i++){
            clients[i].sendUTF(message.utf8Data);
        }
    })
    //close事件表示使用者中斷了連線
    connection.on('close', function (reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');



    })


});

//執行時分別將webServer及SocketServer啟動

//webServer: npm start
//socketServer: node socket_server start